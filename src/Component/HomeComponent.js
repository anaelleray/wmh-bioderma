//Component Home
import { React, Component } from "react";
import { Container, Row, Col, Card } from "react-bootstrap";
import Button from 'react-bootstrap/Button';
import '../style/home.css'
class HomeComponent extends Component {
	render() {
		return (
			<Container fluid>
				<Row id="background-row">
					<Row style={{marginBottom:-4}}>
						<Col>
							<img alt="" src={"./assets/COMPO.png"} style={{ height: 450, width: 500, marginBottom: 0, paddingBottom: 0 }} />
						</Col>
					</Row>
					<Row id="home-text-row">
						<Col >
							<p id="home-title">e-Symposium</p>
							<p id="home-text1">THE SKIN BARRIER, DYNAMIC ECOSYSTEM:</p>
							<p id="home-text2">Acting On Root Causes For a Long-Term Efficacy</p>
						</Col>
						<Col >
							<div style={{ border: "2px solid #4F6884 ", width: 20}}></div>
						</Col>
						<Col id="col-text">
							<p id="home-text3">Tuesday, October 6th, 2020</p>
							<p id="home-text4">7pm to 8:30pm (CEST - Paris Time)</p>
							<Button id="register-btn">Register {'>>'}</Button>
						</Col>
						<Row id="home-card-row">
							<Card class="home-card" style={{ width: '8rem', display: "flex", alignItems:"center", flexDirection: "column"}}>
								<Card.Img variant="top" src="./assets/Amy Paller.png" style={{height:70, width:70}} />
								<Card.Body>
									<Card.Title class="card-title">AMY PALLER</Card.Title>
									<Card.Subtitle class="card-subtitle">(USA)</Card.Subtitle>
								</Card.Body>
							</Card>
							<Card class="home-card" style={{ width: '8rem', display: "flex", alignItems:"center", flexDirection: "column"}}>
								<Card.Img variant="top" src="./assets/Mauro Picardo.png" style={{height:70, width:70,alignSelf:'center'}}/>
								<Card.Body>
									<Card.Title class="card-title">MAURO PICARDO</Card.Title>
									<Card.Subtitle class="card-subtitle">(ITALY)</Card.Subtitle>
								</Card.Body>
							</Card>
							<Card class="home-card" style={{ width: '8rem', display: "flex", alignItems:"center", flexDirection: "column"}}>
								<Card.Img variant="top" src="./assets/Rodrigo Roldan-Marin.png" style={{height:70, width:70}} />
								<Card.Body>
									<Card.Title class="card-title">RODRIGO ROLDÁN-MARIN</Card.Title>
									<Card.Subtitle class="card-subtitle">(MEXICO)</Card.Subtitle>
								</Card.Body>
							</Card>
						</Row>
					</Row>
				</Row>
			</Container>
		)
	}
}
export default HomeComponent;