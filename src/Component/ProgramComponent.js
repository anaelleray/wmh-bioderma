//Component Program
import { React, Component } from "react";
import { Container, Row, Col, Table } from "react-bootstrap";
import '../style/program.css'
class ProgramComponent extends Component {
	render() {
		return (
			<Container fluid>
				<Row id="program-background">
					<Col>
						<Table responsive="sm" striped bordered hover id="table">
							<thead id="head-table">
								<tr id="div-title">
									<th >
										<img alt="" src={'./assets/paper.png'} style={{ height: 20, width: 20, marginRight: 10 }}></img>
									</th>
									<th id="program-title">Program</th>
								</tr>
							</thead>
							<tbody>
								<ul style={{ marginBottom: 10, display: "inline" }}>
									<li style={{ fontFamily: "ITC-Bold", color: "#414042", fontSize: 13, listStyle: "none", marginBottom: 0, marginLeft: 10 }}>•Introduction speech</li>
								</ul>
								<tr style={{ backgroundColor: "#4F6884" }} class="tr-text">
									<td>Paris Time</td>
									<td>NYC Time</td>
									<td>Mexico City Time</td>
									<td>Dubai Time</td>
								</tr>
								<tr style={{ backgroundColor: "#CED5EB" }} class="tr-sub-text">
									<td>7 pm (CEST)</td>
									<td>1:00 pm (EDT)</td>
									<td>12:00 pm (CDT)</td>
									<td>9:00 pm (GST)</td>
								</tr>
								<ul class="program-text-ul">
									<li><span style={{ fontFamily: "ITC-Bold", color: "#3F3E42" }}>• Pr. Amy Paller (USA) </span>What's new on the skin barrier: updates on a dynamic ecosystem interacting with its environment</li>
									<br />
									<li > <span style={{ fontFamily: "ITC-Bold", color: "#3F3E42" }}>• Pr. Mauro Picardo (Italy) </span>The sebaceous gland, a key element<br /> of the skin barrier function: its rule in inflammatory skin discorders</li>
									<br />
									<li><span style={{ fontFamily: "ITC-Bold", color: "#3F3E42" }}>• Pr. Rodrigo Roldán-Marin (Mexico) </span>Use of reflectance confocal microscopy<br /> to assess alterations of the skin barrier: results of a randomized<br /> controlled trial on atopic dermatitis</li>
									<br />
									<li ><span style={{ fontFamily: "ITC-Bold", color: "#3F3E42" }}>• All speakers </span>Q&A session</li>
									<br />
								</ul>
								<tr style={{ backgroundColor: "#4F6884" }} class="tr-text">
									<td>Paris Time</td>
									<td>NYC Time</td>
									<td>Mexico City Time</td>
									<td>Dubai Time</td>
								</tr>
								<tr style={{ backgroundColor: "#CED5EB" }} class="tr-sub-text">
									<td>8:30 pm (CEST)</td>
									<td>2:30 pm (EDT)</td>
									<td>1:30 pm (CDT)</td>
									<td>10:30 pm (GST)</td>
								</tr>
							</tbody>
						</Table>
					</Col>
				</Row>
			</Container>
		)
	}
}
export default ProgramComponent;