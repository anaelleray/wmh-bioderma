//Fichier d'affichage conditionnel pour le texte situé sur le Header
import { useLocation } from 'react-router-dom'
import { React } from 'react';
import { Row, Col } from "react-bootstrap";

const Child = () => {
	//Récupérer l'url par le Router
	const location = useLocation();
	if (location.pathname === "/program") {
		return (
			<Row id="row-text-header">
				<Col >
					<p id="header-title">BIODERMA e-Symposium</p>
				</Col>
				<Col >
					<div style={{ border: "2px solid #4F6884 ", width: 25, marginLeft: 20, marginRight: 20 }}></div>
				</Col>
				<Col id="header-col-text">
					<p id="header-text1">THE SKIN BARRIER, DYNAMIC ECOSYSTEM:</p>
					<p id="header-text2">Acting On Root Causes For a Long-Term Efficacy</p>
					<p id="header-text3">Tuesday, October 6th, 2020 - 7 pm (CEST)
					</p>
				</Col>
			</Row>
		)
	} else {
		return (
			null
		)
	}
}
export default Child