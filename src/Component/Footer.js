//Component Footer
import { React, Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import '../style/footer.css';
class Footer extends Component {
	render() {
		return (
			<Container>
				<Row id="row-text">
					<Col>
						<p>BIODERMA is a brand built on ecobiology which is at the heart of NAOS approach to respect <br></br> the skin ecosystem and preserve its health. Lastingly. <span style={{ fontWeight: "bold", fontFamily: "ITC-Bold", color: "#414042" }}>www.naos.com</span></p>
					</Col>
				</Row>
				<Row id="row-footer">
					<Col>
						<p>Care first.</p>
					</Col>
					<Col>
						<img alt="" src={"./assets/NAOS.png"} />
					</Col>
				</Row>
			</Container>
		)
	}
} export default Footer;