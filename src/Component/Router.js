//Router navigation
import React from "react";
import {
	BrowserRouter as Router,
	Routes,
	Route,
	Link,
} from "react-router-dom";
import HomeComponent from './HomeComponent';
import ProgramComponent from './ProgramComponent';
import Header from './Header'
import "../style/router.css"

export default function Navbar() {
	return (
		<Router>
			<Header />
			<ul id="list">
				<li>
					<Link to="/home" class="list-items" >HOME</Link>
				</li>
				<li>
					<Link to="/registration" class="list-items">REGISTRATION</Link>
				</li>
				<li>
					<Link to="/program" class="list-items">PROGRAM</Link>
				</li>
				<li>
					<Link to="/speakers" class="list-items">SPEAKERS</Link>
				</li>
				<li>
					<Link to="/replay" class="list-items">REPLAY</Link>
				</li>
			</ul>
			<React.Fragment>
				<Routes>
					<Route exact path="/" element={<HomeComponent />}>
					</Route>
					<Route path="/home" element={<HomeComponent />} >
					</Route>
					<Route path="/program" element={<ProgramComponent />}>
					</Route>
				</Routes>
			</React.Fragment>
		</Router>
	);
}