//Component Header
import { React, Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import '../style/header.css';
import TextDisplay from './DisplayText'
class Header extends Component {
	render() {
		return (
			<Container fluid id="container">
				<Row id="row-logo">
					<Col>
						<img alt="" src={"./assets/logo-BIODERMA-gris-Haute-Def.png"} style={{ height: 70, width: 250 }} />
					</Col>
				</Row>
				<TextDisplay></TextDisplay>
			</Container>
		)
	}
}
export default Header;