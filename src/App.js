import React from "react";
import './App.css';
import Router from './Component/Router'
import Footer from './Component/Footer'

function App() {
  return (
    <div className="App">
        <Router></Router>
        <Footer></Footer>
      
    </div>
  );
}

export default App;
